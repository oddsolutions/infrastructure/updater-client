export const API_HOSTNAME = import.meta.env.VITE_API_HOSTNAME
export const BRAND_NAME = import.meta.env.VITE_BRAND_NAME
export const WIKI_URL = import.meta.env.VITE_WIKI_URL
export const HIDE_VERIFIER = import.meta.env.VITE_HIDE_VERIFIER
